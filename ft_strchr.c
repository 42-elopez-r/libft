/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 15:28:26 by elopez-r          #+#    #+#             */
/*   Updated: 2019/11/08 10:56:56 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strchr(const char *s, int c)
{
	int keep;

	keep = 1;
	while (keep)
	{
		if ((unsigned char)*s == (unsigned char)c)
			return ((char*)s);
		if (!*s)
			keep = 0;
		s++;
	}
	return (NULL);
}
